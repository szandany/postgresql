-- liquibase formatted sql
--changeset SteveZ:45679-proc_insert_data_colors splitStatements:false
CREATE PROCEDURE insert_data_colors (a integer, b integer)
LANGUAGE SQL
AS $$
INSERT INTO colors (bcolor, fcolor)
VALUES
  ('red', 'red'),
  ('red', 'red'),
  ('red', NULL),
  (NULL, 'red'),
  ('red', 'green')
$$;
--rollback DROP PROCEDURE insert_data_colors;
